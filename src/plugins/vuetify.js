import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import 'vuetify/dist/vuetify.min.css';
import colors from 'vuetify/es5/util/colors';

Vue.use(Vuetify, {
  theme: {
    primary: colors.indigo.base,
    secondary: colors.indigo.base,
    accent: colors.indigo.base,
  },
  icons: {
    iconfont: 'mdiSvg',
  },
});

export default new Vuetify({});
