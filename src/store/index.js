import Vue from 'vue';
import Vuex from 'vuex';

import UIStore from '@/views/ui/store';
import AuthStore from '@/views/auth/store';
import DashboardStore from '@/views/dashboard/store';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    ui: UIStore,
    auth: AuthStore,
    dashboard: DashboardStore,
  },
});

export default store;
