import axios from 'axios';
import { setToken } from '@/utils';

export default {
  async login({ commit }, loginData) {
    commit('startLogin');
    try {
      const { data } = await axios.post('/auth', { ...loginData });

      if (data.token) {
        setToken(data.token);
        window.location = '/dashboard';
      }
    } catch (err) {
      throw new Error(err);
    } finally {
      commit('endLogin');
    }
  },
};
