import { hasToken } from '@/utils';

export default {
  checkAuth(state) {
    state.isLoggedIn = hasToken();
    state.isAuthChecked = true;
  },

  startLogin(state) {
    state.isFormSubmit = true;
  },

  endLogin(state) {
    state.isFormSubmit = false;
  },

  logErrorMessage(state, error) {
    state.loginErrorMessage = error;
  },
};
