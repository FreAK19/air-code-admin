import actions from './actions';
import mutations from './mutations';

export default {
  state: {
    isAuthChecked: false,
    isLoggedIn: false,
    isFormSubmit: false,
    loginErrorMessage: '',
  },
  mutations,
  actions,
};
