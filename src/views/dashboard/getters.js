export default {
  getMails: state => state.mails,
  getPosts: state => state.posts,
  getCategories: state => state.categories,
  technologies: state => state.categories.content.map(item => item.name),
  isFetching: state => state.fetching,
};
