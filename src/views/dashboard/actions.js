import axios from 'axios';

export default {
  async fetchMails({ commit }) {
    commit('startFetch');
    try {
      const { data } = await axios.get('/mails');
      commit('setMails', data);
    } finally {
      commit('endFetch');
    }
  },

  async fetchCategories({ commit }) {
    commit('startFetch');
    try {
      const { data } = await axios.get('/categories');
      commit('setCategories', data);
    } finally {
      commit('endFetch');
    }
  },

  async fetchPosts({ commit }) {
    commit('startFetch');
    try {
      const { data } = await axios.get('/posts');
      commit('setPosts', data);
    } finally {
      commit('endFetch');
    }
  },

  async createPost({ commit }, card) {
    commit('startFetch');
    try {
      const { data } = await axios.post('/posts', { ...card });
      commit('createPost', data);
    } finally {
      commit('endFetch');
    }
  },

  async createCategory({ commit }, { name }) {
    commit('startFetch');
    try {
      const { data } = await axios.post('/categories', { name });
      commit('createCategory', data);
    } catch (err) {
      throw new Error(err);
    } finally {
      commit('endFetch');
    }
  },

  async updateCategory({ commit }, { name, id }) {
    commit('startFetch');
    try {
      const { data } = await axios.put(`/categories/${id}`, { name });
      commit('createCategory', data);
    } finally {
      commit('endFetch');
    }
  },

  async deletePost({ commit }, postId) {
    commit('startFetch');
    try {
      await axios.delete(`/posts/${postId}`);
      commit('deletePost', postId);
      commit('hideModal');
    } finally {
      commit('endFetch');
    }
  },

  async deleteCategory({ commit }, categoryId) {
    commit('startFetch');
    try {
      await axios.delete(`/categories/${categoryId}`);
      commit('deleteCategory', categoryId);
    } finally {
      commit('endFetch');
    }
  },

  async updatePost({ commit }, payload) {
    commit('startFetch');
    try {
      const { data } = await axios.put(`/posts/${payload.postId}`, { ...payload.card });
      commit('updatePost', data);
      commit('hideModal');
    } finally {
      commit('endFetch');
    }
  },
};
