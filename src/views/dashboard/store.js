import actions from './actions';
import getters from './getters';
import mutations from './mutations';

export default {
  state: {
    posts: {
      totalElements: 0,
      content: [],
    },
    mails: {
      totalElements: 0,
      content: [],
    },
    categories: {
      totalElements: 0,
      content: [],
    },
    fetching: false,
  },
  mutations,
  actions,
  getters,
};
