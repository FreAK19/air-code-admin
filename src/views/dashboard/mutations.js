export default {
  setMails(state, payload) {
    state.mails = payload;
  },

  setCategories(state, payload) {
    state.categories = payload;
  },

  setPosts(state, payload) {
    state.posts = payload;
  },

  startFetch(state) {
    state.fetching = true;
  },

  endFetch(state) {
    state.fetching = false;
  },

  createPost(state, payload) {
    const newState = state.posts;
    newState.content.push(payload);
    newState.totalElements = state.posts.totalElements + 1;
    state.posts = newState;
  },

  deletePost(state, payload) {
    const newState = state.posts;
    const newPosts = newState.content.filter(post => post._id !== payload);
    newState.content = newPosts;
    newState.totalElements = state.posts.totalElements - 1;
    state.posts = newState;
  },

  updatePost(state, payload) {
    const newState = state.posts;
    const postIndex = newState.content.findIndex(item => item._id === payload._id);
    newState.content.splice(postIndex, 1, payload);
    state.posts = newState;
  },

  createCategory(state, payload) {
    const newState = state.categories;
    newState.content.unshift(payload);
    newState.totalElements = state.categories.totalElements + 1;
    state.categories = newState;
  },

  deleteCategory(state, payload) {
    const newState = state.categories;
    const newCategories = newState.content.filter(item => item._id !== payload);
    newState.content = newCategories;
    newState.totalElements = state.categories.totalElements - 1;
    state.categories = newState;
  },

  updateCategory(state, payload) {
    const newState = state.categories;
    const postIndex = newState.content.findIndex(item => item._id === payload._id);
    newState.content.splice(postIndex, 1, payload);
    state.categories = newState;
  },
};
