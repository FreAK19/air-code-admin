export default {
  showModal(state, payload) {
    state.modalType = payload.modalType;
    state.modalProps = payload.modalProps;
  },
  hideModal(state) {
    state.modalType = null;
    state.modalProps = {};
  },
};
