import mutations from './mutations';

const UIStore = {
  state: {
    modalType: null,
    modalProps: {},
  },
  mutations,
};

export default UIStore;
