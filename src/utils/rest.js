import axios from 'axios';
import config from '@/config';
import { getToken } from '@/utils';

export const initRestParams = () => {
  const token = getToken();
  const url = config[process.env.NODE_ENV].API_DOMAIN;

  axios.defaults.baseURL = url;

  if (token) {
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  }
};
