import { initRestParams } from '@/utils';

const store = window.localStorage;

export const setToken = token => store.setItem('token', token);

export const getToken = () => store.getItem('token');

export const hasToken = () => Boolean(getToken());

export const removeToken = () => {
  store.removeItem('token');
};

export const signOut = (cb = () => {}) => {
  removeToken();
  initRestParams();
  cb();
};
