import Vue from 'vue';
import { loadProgressBar } from 'axios-progress-bar';

import App from '@/App.vue';
import router from '@/router';
import store from '@/store';
import '@/plugins/notifications';
import vuetify from '@/plugins/vuetify';
import { initRestParams } from '@/utils';
import 'axios-progress-bar/dist/nprogress.css';

Vue.config.productionTip = false;

initRestParams();

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App),
}).$mount('#app');

loadProgressBar();
