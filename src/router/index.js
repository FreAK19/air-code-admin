import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import LoginPage from '@/views/auth/containers/Login';
import PdfPage from '@/views/dashboard/containers/PdfPage';
import Dashboard from '@/views/dashboard/containers/Dashboard';
import ContactsPage from '@/views/dashboard/containers/Contacts';
import CardAddPage from '@/views/dashboard/containers/CardAddPage';
import CategoriesPage from '@/views/dashboard/containers/Categories';
import DashboardPostList from '@/views/dashboard/containers/DashboardPostListPage';
import DashboardMailsList from '@/views/dashboard/containers/DashboardMailsListPage';

const routes = [
  { path: '/', exact: true, component: LoginPage },
  {
    path: '/dashboard',
    exact: true,
    component: Dashboard,
    children: [
      {
        path: 'posts',
        component: DashboardPostList,
      },
      {
        path: 'mails',
        component: DashboardMailsList,
      },
      {
        path: 'categories',
        component: CategoriesPage,
      },
      {
        path: 'pdf',
        component: PdfPage,
      },
      {
        path: 'add-post',
        component: CardAddPage,
      },
      {
        path: 'contacts',
        component: ContactsPage,
      },
    ],
  },
];

const router = new VueRouter({
  mode: 'history',
  routes,
});

export default router;
