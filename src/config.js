export default {
  production: {
    API_DOMAIN: 'https://pure-eyrie-10747.herokuapp.com/api',
  },
  staging: {
    API_DOMAIN: 'https://pure-eyrie-10747.herokuapp.com/api',
  },
  development: {
    API_DOMAIN: 'http://localhost:3003/api',
  },
};
